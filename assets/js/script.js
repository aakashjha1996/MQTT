function getLoader(topic) {
    UIkit.notification("<div uk-spinner></div> "+ topic +"... !", {timeout:'20000'});
}

function processForm(e) {
    e.preventDefault();
    getLoader("Publishing");
    topic = document.getElementById("publish-topic").value;
    content = document.getElementById("publish-content").value;
    var data = {
        "topic": topic,
        "content": content
    };
    url = "/home/publish/"
    fetch(url, {
        credentials: "same-origin",
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        body: JSON.stringify(data)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(data){
        UIkit.notification.closeAll();
        if (data["info"] == "OK"){
            UIkit.notification("<span uk-icon='icon: check'></span> Published Successfully !");
        }
        else {
            UIkit.notification("<span uk-icon='icon: close'></span> Could not Publish !");
        }
    })
    .catch(function(error){
        console.log("Error : " + error);
    })
}

function websocketSub(e) {
    e.preventDefault();
    getLoader("Subscribing");
    topic = document.getElementById("subscribe-topic").value;
    const webSocketBridge = new channels.WebSocketBridge();
    webSocketBridge.connect('/ws/');
    webSocketBridge.socket.addEventListener('open', function() {
        webSocketBridge.send(topic);
        UIkit.notification.closeAll();
        // UIkit.notification("<span uk-icon='icon: check'></span> Subscribed Successfully !");
        webSocketBridge.listen(function(action, stream) {
            if (action["topic"] == undefined ) {
                UIkit.notification("<a href=''><span uk-icon='icon: check'></span> Already Subscribed !</a>");
            }
            else {
                var topic = action["topic"];
                var payload = action["payload"].substring(2,action["payload"].length-1);
                document.getElementById("topic").value += topic + "\n";
                document.getElementById("msg").value += payload + "\n";
                UIkit.notification("<span uk-icon='icon: bell'></span> New Data Received !");
            }  
          });
        
    })
}

var form2 = document.getElementById("subscribe");
form2.addEventListener("click", websocketSub);


var form1 = document.getElementById("publish");
form1.addEventListener("submit", processForm);