# MQTT Web Client

An web client that can connect to any MQTT broker and send/receive messages using MQTT protocol in real time using Django Channels. 

### Things used to build this:
- Django
- Paho MQTT Library
- Django Channels
- UIKit
- ChartJs

### How to use:

- Create an account on https://www.cloudmqtt.com/
- Clone the repo.
- In snippets/consumers.py, add username, password, broker address and port number as mentioned in the CloudMQTT website.
- Run <code>pip install -r requirements.txt</code> on root directory of project.
- Run <code>python manage.py</code> on root directory of project.