from channels import Group
import json
import paho.mqtt.client as mqtt
import threading

all_topics = []
kill = 0

class SubscribeThread(threading.Thread):
    
    def __init__(self, topic):
        threading.Thread.__init__(self)
        self.mqttclient = mqtt.Client()
        self.mqttclient.on_connect = on_connect
        self.mqttclient.on_subscribe = on_subscribe
        self.mqttclient.on_message = on_message
        # set username and password respectively
        self.mqttclient.username_pw_set("", "")
        # set broker address as strinf and port as integer accordingly
        self.mqttclient.connect("", )
        self.topic = topic

    def run(self):
        self.mqttclient.subscribe(self.topic)
        keep_alive(self.mqttclient,self.topic)


def keep_alive(mqttclient,topic):
    rc = 0
    while rc == 0 and kill != 1:
        rc = mqttclient.loop()
    if kill == 1:
        mqttclient.unsubscribe(topic)


def on_connect(client, userdata,flag, rc):
    pass

def on_subscribe(client, userdata, mid, granted_qos):
    pass

def on_message(client, userdata, msg):
    send_message(str(msg.topic),str(msg.payload))


def ws_connect(message):
    message.reply_channel.send({"accept": True})

def isPresent(topic):
    for item in all_topics:
        if item == topic:
            return 0
    all_topics.append(topic)
    return 1

def ws_message(message):
    topic = message.content["text"].strip("\"")
    if isPresent(topic) == 1:
        Group('subscribe-%s' % topic).add(message.reply_channel)
        thread1 = SubscribeThread(topic)
        thread1.start()
    else:
        message.reply_channel.send({"text": json.dumps({"status": "Done"})})
        

def send_message(topic,payload):
    Group("subscribe-%s" % topic).send({
        'text': json.dumps({
            "topic": topic,
            "payload": payload
        })
    })

def ws_disconnect(message):
    isPresent = []
