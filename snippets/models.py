from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles


class Ldr(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    value = models.IntegerField(default=0)