from rest_framework import serializers
from snippets.models import Ldr


class LdrSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ldr
        fields = ('id','created','value')
