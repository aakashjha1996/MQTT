from django.shortcuts import render, get_object_or_404
from django.utils.six import BytesIO
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import status
from snippets.models import Ldr
from snippets.serializers import LdrSerializer
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect, ensure_csrf_cookie
import paho.mqtt.client as mqtt
import threading
from channels import Channel
limit = 5

class PublishThread(threading.Thread):
    def __init__(self, topic, message):
        threading.Thread.__init__(self)
        self.mqttclient = mqtt.Client()
        self.mqttclient.on_connect = on_connect
        self.mqttclient.on_publish = on_publish
        self.mqttclient.username_pw_set("buejxdyr", "14_7MM9-anPM")
        self.mqttclient.connect("m14.cloudmqtt.com", 10635)
        self.topic = topic
        self.message = message
    
    def run(self):
        self.mqttclient.publish(self.topic, self.message)

def on_connect(client, userdata,flag, rc):
    if rc == 0:
        print("Connected successfully.")
    else:
        print("Connection failed. rc= "+str(rc))

def on_publish(client, userdata, mid):
    print("Message "+str(mid)+" published.")

def home(request):
    return render(request,'mqtt.html')

@ensure_csrf_cookie
def publish(request):
    if request.method == 'POST':
        try:
            stream = BytesIO(request.body)
            data = JSONParser().parse(stream)
            thread1 = PublishThread(data['topic'],data['content'])
            thread1.start()
            return JsonResponse({"info":"OK"})
        except:
            return JsonResponse({"info":"Error"})
    else:
        return JsonResponse({"info":"Error"})