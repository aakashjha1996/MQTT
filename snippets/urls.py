from django.conf.urls import url,include
from snippets import views

urlpatterns = [
    url(r'home/$',views.home),
    url(r'home/publish/$',views.publish)
]